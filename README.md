# 1.-Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml 
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
  .orange {
    BackgroundColor #f33501
  }
}
</style>
* Programación Declarativa. \n Orientaciones y pautas para el estudio
** Consiste en <<green>>
*** Decirle a la máquina lo que se requiere que haga <<blue>>
** Evolucion <<green>>
***_ al principio  <<blue>>
**** Los programas eran ordenes <<rose>>
***** Directas <<orange>>
***** Sencillas <<orange>>
***** Concretas <<orange>>
**** Los lenguajes eran <<rose>>
***** Alto nivel <<orange>>
***** Intermedio <<orange>>
***** Bajo nivel <<orange>>
** Tiene 2 partes <<green>>
*** Creativa <<blue>>
****_ consiste en <<rose>>
***** Formas de solucion <<orange>>
***** Ideas <<orange>>
***** La creación del algoritmo <<orange>>
*** Burocratica <<blue>>
****_ consiste en <<rose>>
***** La gestion detallada de la memoria <<orange>>
***** Secuencia de las ordenes <<orange>>
** Tipos <<green>>
*** Surgen por <<blue>>
**** Problemas que existen en la programacion clasica <<rose>>
*** Declarativa  <<blue>>
**** Consiste en <<rose>>
***** Hacer enfasis en aspectos creativos para resolver problemas <<orange>>
***** Abandonar las secuencias de órdenes que gestionan la memoria del ordenador <<orange>>
**** Dos tipos de variantes <<rose>>
***** Funcional <<blue>>
****** Consite en <<green>>
******* Tomar el modelo en el cual los matemáticos definen las funciones <<blue>>
********_ por medio de
********* Reglas de reescritura <<orange>>
********* Simplificar expresiones <<orange>>
****** Caracteristicas <<green>>
******* Define tipos de datos infinitos <<blue>>
******* Funciones de orden superior <<blue>>
********_ tienen como objetivo
********* Tomar una o más funciones como entrada <<orange>>
********* Devolver una función como salida  <<orange>>
****** Lenguajes <<green>>
******* Hibridos <<blue>>
******** LISP <<rose>>
********* Permite hacer un tipo de programación imperativa <<orange>>
******* Estándar <<blue>>
******** PROLOG <<rose>>
********* Representacion del paradigma declarativo <<orange>>
******** HASKELL <<rose>>
********* Se utiliza en sistemas diferenciales <<orange>>
********* Evaluacipon perezosa <<orange>>
***** Lógica <<blue>>
****** Consiste en <<green>>
******* Acudir a la lógica de predicados de primer orden <<blue>>
********_ crea
********* Funciones que actúan sobre <<rose>>
********** Datos primitivos  <<orange>>
********** Funciones <<orange>>
****** Caracteristicas <<green>>
******* Programación cómoda  <<orange>>
******* Programación perezosa <<orange>>
******* No establece orden entre argumentos de entrada y datos de salida <<orange>>
****** Ventajas <<green>>
******* Permite ser mas declarativo <<blue>>
**** Objetivo <<rose>>
***** Reduccion de errores al programar <<blue>>
***** Crear Programas <<blue>>
****** Faciles de depurar <<orange>>
****** Cortos <<orange>>
****** Faciles de realizar <<orange>>
***** Refuccion de la complejidad de los programas <<blue>>
***** Librarse de las asignaciones y el detallado del control de la gestión de memoria <<blue>>
**** Ventajas <<rose>>
***** Tiempo de desarrollo más corto  <<orange>>
***** Tiempo de modificación y depuración corto <<orange>>
*** Imperativa <<blue>>
**** Consiste en <<rose>>
***** Dar instruccione, comandos uno detras de otro al ordenador <<blue>>
**** Existen diferentes tipos <<rose>> 
***** Estructurad <<orange>>
***** Procedimental <<orange>>
***** Modular <<orange>>
*** Operativa <<blue>>
**** Se basa en el modelos de Von Neumann <<rose>>
@endmindmap
```
# 2.-Lenguaje de Programación Funcional (2015)
```plantuml 
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
  .orange {
    BackgroundColor #f33501
  }
}
</style>
* Programación Funcional
** Memorización <<green>>
*** Consiste en <<blue>>
**** Almacenar el valor de una expresión cuya evaluacion fue realizada <<rose>>
*** Aportado por <<blue>>
**** Donald Michie <<rose>>
***_ En <<blue>>
**** 1968 <<rose>>
** Currificación <<green>>
*** Conssite en <<blue>>
**** En recibir varias fuciones de entrada y devolver diferentes salirdas <<rose>>
*** En honor a <<blue>>
**** Haskell Brooks <<rose>>
** Evaluación <<green>>
*** Se divide en 3 partes <<blue>>
**** Estricta <<rose>>
***** Evaluar lo más interno despues lo más externo <<orange>>
**** No estricta <<rose>>
***** Evaluar de afuera hacia adentro <<orange>>
***** Mas compleja de implementar <<orange>>
**** En corto circuito <<rose>>
***** Emplea operadores boleanos <<orange>>
** Cálculo Lambda <<green>>
***_ Es <<blue>>
**** Sistema computacional bastante potente <<rose>>
***** Equivalente al propuesto por Von Newman <<orange>>
*** Creado por <<blue>>
**** Alonzo Church <<rose>>
**** Stephen Kleene <<rose>>
*** Desarrolado década de 1930 <<blue>>
*** Se crea <<blue>>
**** ALGOL <<rose>>
*****_ Por 
****** Peter J. Landin <<orange>>
** Caracteristicas <<green>>
*** Estado del computo <<blue>>
*** Aplicacion de una función con menos parámetros <<blue>>
*** Transparencia referencial <<blue>>
*** El resultado de las funciones no dependen de otras <<blue>>
** Funciones <<green>>
*** Funciones constantes <<blue>>
**** Devuelven siempre el mismo resultado <<rose>>
*** Funciones matematicas <<blue>>
**** Dan como resulado <<rose>>
***** Recursión <<orange>>
***** Concepto de asignación <<orange>>
***** Concepto que desaparecen <<orange>>
****** Variable <<orange>>
******* Sirven para referirse a los parametros de entrada de las funciones <<green>>
****** Bucle <<orange>>
******* Funciones recursivas <<green>>
******** Aquellas donde se hace referencia a ellas mismas <<blue>>
*** Funciones de orden superior <<blue>>
** Ventajas <<green>>
*** No exsiste la dependencia entre las funciones <<blue>>
*** Proporciona un entorno multiprocesador <<blue>>
*** Se puede escribir codigo mas rapido <<blue>>
** Desventajas <<green>>
*** Se requiere un nivel de abstracción demaciado alto <<blue>>
@endmindmap
```